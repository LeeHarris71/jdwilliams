package com.jdwilliams.formula1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import com.jdwilliams.formula1.Factory.DriverFactory;
import com.jdwilliams.formula1.Factory.DriverFactoryImpl;
import com.jdwilliams.formula1.model.Driver;

public class TestDriverFactory {

	
	Document document;
	@Before
	public void setupDocument() throws IOException{
		
		 document = Jsoup.parse(this.getClass().getResourceAsStream("/DriverTest.html"),
				null, "/");
		
	}
	
	
	@Test
	public void testCreateDrivers() {
	
		
		DriverFactory factory = new DriverFactoryImpl(".msr_season_driver_results tbody tr",".msr_driver a",".msr_total");
		
		List<Driver> list = factory.createDrivers(document);
		
		assertEquals( "DRIVER 1",list.get(0).getName() );
		assertEquals( "DRIVER 2",list.get(1).getName() );
		assertEquals( "DRIVER 3",list.get(2).getName() );
		assertEquals( "DRIVER 4",list.get(3).getName() );
		
		assertEquals( new Integer(74),list.get(0).getPoints() );
		assertEquals( new Integer(201),list.get(1).getPoints() );
		assertEquals( new Integer(102),list.get(2).getPoints() );
		assertEquals( new Integer(23),list.get(3).getPoints() );
		
	}
	

	@Test
	public void testCreateAndSortDriverByPoints() {
	
		
		DriverFactory factory = new DriverFactoryImpl(".msr_season_driver_results tbody tr",".msr_driver a",".msr_total");
		
		List<Driver> list = factory.createAndSortDriversByPoints(document, 3);
		
		assertEquals( "DRIVER 2",list.get(0).getName() );
		assertEquals( "DRIVER 3",list.get(1).getName() );
		assertEquals( "DRIVER 1",list.get(2).getName() );
	
		
		assertEquals( new Integer(201),list.get(0).getPoints() );
		assertEquals( new Integer(102),list.get(1).getPoints() );
		assertEquals( new Integer(74),list.get(2).getPoints() );
		
	}
	
	@Test
	public void testCreateAndSortDriverByPointsWithMaxSizeGreaterThanListSize() {
	
		
		DriverFactory factory = new DriverFactoryImpl(".msr_season_driver_results tbody tr",".msr_driver a",".msr_total");
		
		List<Driver> list = factory.createAndSortDriversByPoints(document, 10);
		

		assertEquals( "DRIVER 2",list.get(0).getName() );
		assertEquals( "DRIVER 3",list.get(1).getName() );
		assertEquals( "DRIVER 1",list.get(2).getName() );
		assertEquals( "DRIVER 4",list.get(3).getName() );
		
		assertEquals( new Integer(201),list.get(0).getPoints() );
		assertEquals( new Integer(102),list.get(1).getPoints() );
		assertEquals( new Integer(74),list.get(2).getPoints() );
		assertEquals( new Integer(23),list.get(3).getPoints() );
		
	}

	
	

}
