package com.jdwilliams.formula1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import com.jdwilliams.formula1.Factory.TeamFactory;
import com.jdwilliams.formula1.Factory.TeamFactoryImpl;
import com.jdwilliams.formula1.model.Team;

public class TestTeamFactory {

	Document document;
	TeamFactory factory;

	@Before
	public void setupDocument() throws IOException {

		this.document = Jsoup.parse(
				this.getClass().getResourceAsStream("/TeamTest.html"), null,
				"/");

		this.factory = new TeamFactoryImpl(".msr_season_team_results tbody tr", ".msr_team a",
				".msr_total");

	}

	@Test
	public void testCreateTeams() {

		List<Team> list = factory.createTeams(document);

		assertEquals(list.get(0).getName(), "TEAM 1");
		assertEquals(list.get(1).getName(), "TEAM 2");
		assertEquals(list.get(2).getName(), "TEAM 3");
		assertEquals(list.get(0).getPoints(),  new Integer("234"));
		assertEquals(list.get(1).getPoints(),  new Integer("345"));
		assertEquals(list.get(2).getPoints(),  new Integer("567"));
		

	}

	@Test
	public void testCreateAndSortTeamByPoints() {

		List<Team> list = factory.createAndSortTeamsByPoints(document, 3);

		assertEquals(list.get(0).getName(), "TEAM 3");
		assertEquals(list.get(1).getName(), "TEAM 2");
		assertEquals(list.get(2).getName(), "TEAM 1");
		assertEquals(list.get(0).getPoints(), new Integer("567"));
		assertEquals(list.get(1).getPoints(),  new Integer("345"));
		assertEquals(list.get(2).getPoints(),  new Integer("234"));

	}

	@Test
	public void testCreateAndSortTeamByPointsWithMaxSizeGreaterThanListSize() {

		TeamFactoryImpl factory = new TeamFactoryImpl(".msr_season_team_results tbody tr",
				".msr_team a", ".msr_total");

		List<Team> list = factory.createAndSortTeamsByPoints(document, 10);

		assertEquals(list.get(0).getName(), "TEAM 3");
		assertEquals(list.get(1).getName(), "TEAM 2");
		assertEquals(list.get(2).getName(), "TEAM 1");
		assertEquals(list.get(0).getPoints(),  new Integer("567"));
		assertEquals(list.get(1).getPoints(),  new Integer("345"));
		assertEquals(list.get(2).getPoints(),  new Integer("234"));

	}

}
