package com.jdwilliams.formula1;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.jdwilliams.formula1.model.Driver;
import com.jdwilliams.formula1.model.Results;
import com.jdwilliams.formula1.model.Team;
import com.jdwilliams.formula1.service.ResultsConverter;
import com.jdwilliams.formula1.service.ResultsConverterImpl;

public class TestResultsConverter {

	@Test
	public void test() {
		
		ResultsConverter converter = new ResultsConverterImpl();
		
		Results results = new Results();
		
		List<Driver> drivers = new ArrayList<Driver>();
		
		Driver driver = new Driver();
		driver.setName("driver 1");
		driver.setPoints(new Integer("123"));
		drivers.add(driver);
		
 		List<Team> teams = new ArrayList<Team>();
 		Team team = new Team();
 		team.setName("Team 1");
 		team.setPoints(new Integer("345"));
 		teams.add(team);
 		
		results.setDrivers(drivers);
		results.setTeams(teams);
		
		assertEquals("{\"drivers\":[{\"name\":\"driver 1\",\"points\":123}],\"teams\":[{\"name\":\"Team 1\",\"points\":345}]}",converter.convertToJson(results));
		
		
	
	}

}
