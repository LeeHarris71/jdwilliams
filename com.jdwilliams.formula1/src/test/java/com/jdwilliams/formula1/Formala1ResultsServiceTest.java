package com.jdwilliams.formula1;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import com.jdwilliams.formula1.Factory.DriverFactory;
import com.jdwilliams.formula1.Factory.DriverFactoryImpl;
import com.jdwilliams.formula1.Factory.TeamFactoryImpl;
import com.jdwilliams.formula1.model.Results;
import com.jdwilliams.formula1.service.Formula1ResultsServiceImpl;
import com.jdwilliams.formula1.service.HTMLParser;

public class Formala1ResultsServiceTest {

	@Test
	public void test() {

		
		HTMLParser parser = new HTMLParser() {
			
			@Override
			public Document getHTMLDocument() {
				
				try {
					return Jsoup.parse(this.getClass().getResourceAsStream("/FormulaResultsServiceTest.html"),
							null, "/");
				} catch (IOException e) {
					
					throw new RuntimeException();
				}
			}
		};
		
		DriverFactory driverFactory = new DriverFactoryImpl(".msr_season_driver_results tbody tr",".msr_driver a",".msr_total");
		TeamFactoryImpl teamFactory= new TeamFactoryImpl(".msr_season_team_results tbody tr", ".msr_team a",
				".msr_total");

		
		Formula1ResultsServiceImpl resultsService = new Formula1ResultsServiceImpl(parser,driverFactory,teamFactory);
		
		Results results = resultsService.getResults();
	
		assertEquals(results.getTeams().get(0).getName(), "TEAM 3");
		assertEquals(results.getTeams().get(1).getName(), "TEAM 2");
		assertEquals(results.getTeams().get(2).getName(), "TEAM 1");
		assertEquals(results.getTeams().get(0).getPoints(), new Integer("567"));
		assertEquals(results.getTeams().get(1).getPoints(),  new Integer("345"));
		assertEquals(results.getTeams().get(2).getPoints(),  new Integer("234"));
		assertEquals( "DRIVER 2",results.getDrivers().get(0).getName() );
		assertEquals( "DRIVER 3",results.getDrivers().get(1).getName() );
		assertEquals( "DRIVER 1",results.getDrivers().get(2).getName() );
	
		
		assertEquals( new Integer(201),results.getDrivers().get(0).getPoints() );
		assertEquals( new Integer(102),results.getDrivers().get(1).getPoints() );
		assertEquals( new Integer(74),results.getDrivers().get(2).getPoints() );
	}

}
