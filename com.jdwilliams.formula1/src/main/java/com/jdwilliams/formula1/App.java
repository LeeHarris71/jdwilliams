/**
 * 
 */
package com.jdwilliams.formula1;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jdwilliams.formula1.service.Formula1ResultsService;
import com.jdwilliams.formula1.service.ResultsConverter;


/**
 * @author leeharris
 *
 */

public class App {

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				SpringConfig.class);
		
		Formula1ResultsService service = (Formula1ResultsService) context.getBean("formula1ResultsService");
		ResultsConverter converter = (ResultsConverter) context.getBean("resultsConverter");
		
		System.out.println(converter.convertToJson(service.getResults()));
		context.close();
		
	}
	
	
	

}
