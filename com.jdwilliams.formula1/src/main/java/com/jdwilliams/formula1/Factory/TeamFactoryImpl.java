package com.jdwilliams.formula1.Factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jdwilliams.formula1.model.Team;
@Component
public class  TeamFactoryImpl implements TeamFactory {
	
	private String tableElementId; 
	private String nameElementId; 
	private String pointsElementId;
	
	@Override
	public List<Team> createAndSortTeamsByPoints(Document document,int maxSize){
		List<Team> TeamList = this.createTeams(document);
		
		if(maxSize>TeamList.size()){
			maxSize = TeamList.size();
		}
		Collections.sort(TeamList,(d1,d2)->d1.getPoints().compareTo(d2.getPoints()));
		Collections.reverse(TeamList);
		
		
		return TeamList.subList(0, maxSize);
		
		
	}
	
	@Override
	public List<Team> createTeams(Document document){
		
		Elements htmlTeamResults = document.select(tableElementId);
		
		
		List<Team> TeamResults = new ArrayList<Team>(htmlTeamResults.size());
		
		//read through each row of the table and get the name and points elements
		//not all rows have the name and points elements so check we have the element before setting 
		for(Element row: htmlTeamResults){
			Team team = new Team();
			Elements name =row.select(nameElementId);
			Elements points = row.select(pointsElementId);
			
			if (!name.isEmpty() && !points.isEmpty()){
				team.setName(name.first().html());
				team.setPoints(Integer.valueOf(points.first().html()));
				TeamResults.add(team);
			}
		
		}
		
		
		
		return TeamResults;
	}
	@Autowired
	public TeamFactoryImpl(String teamTableElementId, String teamNameElementId,
			String teamPointsElementId) {
		super();
		this.tableElementId = teamTableElementId;
		this.nameElementId = teamNameElementId;
		this.pointsElementId = teamPointsElementId;
	}

	
	
	
	
	
}
