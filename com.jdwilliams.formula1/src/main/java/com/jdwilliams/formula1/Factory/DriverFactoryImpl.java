package com.jdwilliams.formula1.Factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.jdwilliams.formula1.model.Driver;
@Component
public class DriverFactoryImpl implements DriverFactory {
	
	private String tableElementId; 
	private String nameElementId; 
	private String pointsElementId;
	
	@Override
	public List<Driver> createAndSortDriversByPoints(Document document,int maxSize){
		List<Driver> driverList = this.createDrivers(document);
		
		if(maxSize>driverList.size()){
			maxSize = driverList.size();
		}
		Collections.sort(driverList,(d1,d2)->d1.getPoints().compareTo(d2.getPoints()));
		Collections.reverse(driverList);
		
		
		return driverList.subList(0, maxSize);
		
		
	}
	
	@Override
	public List<Driver> createDrivers(Document document){
		
		Elements htmlDriverResults = document.select(tableElementId);
		
		List<Driver> driverResults = new ArrayList<Driver>(htmlDriverResults.size());
		
		for(Element row: htmlDriverResults){
			Driver driver = new Driver();
			driver.setName(row.select(nameElementId).first().html());
			driver.setPoints(Integer.valueOf(row.select(pointsElementId).first().html()));
			driverResults.add(driver);
		}
		
		
		
		return driverResults;
	}

	public DriverFactoryImpl(String driverTableElementId, String driverNameElementId,
			String driverPointsElementId) {
		super();
		this.tableElementId = driverTableElementId;
		this.nameElementId = driverNameElementId;
		this.pointsElementId = driverPointsElementId;
	}

	

	
	
	
	
	

}
