package com.jdwilliams.formula1.Factory;

import java.util.List;

import org.jsoup.nodes.Document;

import com.jdwilliams.formula1.model.Driver;

public interface DriverFactory {
	
	/**
	 * return a list of Drivers from a HTML document
	 * sorted in order of points with a size of max Size
	 * 
	 * @param document
	 * @param maxSize - max size of list to be returned
	 * @return
	 */
	List<Driver> createAndSortDriversByPoints(Document document,int maxSize);
	
	/**
	 * return a list of Drivers from a HTML document. In the order in they appear
	 */
	List<Driver> createDrivers(Document document);
}