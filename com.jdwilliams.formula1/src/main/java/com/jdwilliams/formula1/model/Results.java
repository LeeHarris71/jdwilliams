package com.jdwilliams.formula1.model;

import java.util.List;


/**
 * results data for the season
 * @author leeharris
 *
 */
public class Results {

	//list of drivers
	private List<Driver> drivers;
	
	//list of teams
	private List<Team> teams;

	public List<Driver> getDrivers() {
		return drivers;
	}

	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Drivers/n");
		drivers.stream().forEach(d ->sb.append(d.toString()).append("\n"));
		
		sb.append("Team/n");
		teams.stream().forEach(t ->sb.append(t.toString()).append("\n"));
		
		return sb.toString();
	}
}
