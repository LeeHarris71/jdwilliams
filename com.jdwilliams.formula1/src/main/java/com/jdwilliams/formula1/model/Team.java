package com.jdwilliams.formula1.model;

/**
 * contains information on the team
 * @author leeharris
 *
 */
public class Team {

	private String name;
	private Integer points;
	public Integer getPoints() {
		return points;
	}
	public void setPoints(Integer points) {
		this.points = points;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		return sb.append("name : ").append(name).append(" points : ").append(points).toString();
			
	}
	
}
