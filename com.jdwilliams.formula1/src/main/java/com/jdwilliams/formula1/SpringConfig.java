package com.jdwilliams.formula1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jdwilliams.formula1.Factory.DriverFactory;
import com.jdwilliams.formula1.Factory.DriverFactoryImpl;
import com.jdwilliams.formula1.Factory.TeamFactory;
import com.jdwilliams.formula1.Factory.TeamFactoryImpl;
import com.jdwilliams.formula1.service.Formula1ResultsService;
import com.jdwilliams.formula1.service.Formula1ResultsServiceImpl;
import com.jdwilliams.formula1.service.HTMLParser;
import com.jdwilliams.formula1.service.HttpHtmlParser;
import com.jdwilliams.formula1.service.ResultsConverter;
import com.jdwilliams.formula1.service.ResultsConverterImpl;

@Configuration
public class SpringConfig {

	
	@Bean 
	public HTMLParser parser(){
		HttpHtmlParser parser = new HttpHtmlParser();
		parser.setUrl("http://www.f1-fansite.com/f1-results/2015-f1-results-standings/");
		return parser;
	}
	
	@Bean 
	public DriverFactory driverFactory(){
		return new DriverFactoryImpl(".msr_season_driver_results tbody tr",".msr_driver a",".msr_total");
		
	}
	@Bean
	public TeamFactory teamFactory(){
		return new TeamFactoryImpl(".msr_season_team_results tbody tr", ".msr_team a",
				".msr_total");
	}
	@Bean
	public Formula1ResultsService formula1ResultsService(){
		return new Formula1ResultsServiceImpl();
	}
	@Bean
	public ResultsConverter resultsConverter(){
		return new ResultsConverterImpl();
	}
	
	
}
