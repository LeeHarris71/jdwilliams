/**
 * 
 */
package com.jdwilliams.formula1.service;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

/**
 * @author leeharris
 *
 */
@Component
public class HttpHtmlParser implements HTMLParser {

	
	private String parserUrl;
	
	public void setUrl(String url) {
		this.parserUrl = url;
	}

	/* (non-Javadoc)
	 * @see com.jdwilliams.formula1.service.HTMLParser#getHTMLDocument()
	 */
	@Override
	public Document getHTMLDocument() {
		Document doc;
		try {
			doc = Jsoup.connect(parserUrl).get();
		} catch (IOException e) {
			
			throw new RuntimeException("Error getting the HTML Document", e);
		}
		
		return doc;
	}

}
