package com.jdwilliams.formula1.service;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdwilliams.formula1.model.Results;
@Component
public class ResultsConverterImpl implements ResultsConverter {

	@Override
	public String convertToJson(Results results) {
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonOutput= new String();
		try {
			jsonOutput = mapper.writeValueAsString(results);
		} catch (JsonProcessingException e) {
			
			throw new RuntimeException("Error writing JSON output", e);
		}
		
		return jsonOutput;
	}

}
