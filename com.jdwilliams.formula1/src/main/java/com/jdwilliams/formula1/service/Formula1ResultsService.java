package com.jdwilliams.formula1.service;

import com.jdwilliams.formula1.model.Results;

public interface Formula1ResultsService {
	
	/**
	 * get the seasons results
	 * @return
	 */
	
	public Results getResults();

}
