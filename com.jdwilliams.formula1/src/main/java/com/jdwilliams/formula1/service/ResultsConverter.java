package com.jdwilliams.formula1.service;

import com.jdwilliams.formula1.model.Results;

public interface ResultsConverter {
	
	/**
	 * convert the results object to JSON format
	 * @param results
	 * @return
	 */
	String convertToJson(Results results);

}
