package com.jdwilliams.formula1.service;

import org.jsoup.nodes.Document;


/** Retrieve a HTML page and convert to an object
 * 
 * @author leeharris
 *
 */
public interface HTMLParser {
	
	/**
	 * Retrieve a HTML page and convert to an object
	 * @return an object representing the HTML document
	 */
	Document getHTMLDocument();
	

}
