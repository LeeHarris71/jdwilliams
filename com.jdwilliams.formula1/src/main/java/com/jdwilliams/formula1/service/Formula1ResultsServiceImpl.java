package com.jdwilliams.formula1.service;

import java.util.List;

import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jdwilliams.formula1.Factory.DriverFactory;
import com.jdwilliams.formula1.Factory.TeamFactory;
import com.jdwilliams.formula1.model.Driver;
import com.jdwilliams.formula1.model.Results;
import com.jdwilliams.formula1.model.Team;
@Component
public class Formula1ResultsServiceImpl implements Formula1ResultsService {

	@Autowired
	private HTMLParser parser;
	@Autowired
	private DriverFactory driverFactory;
	@Autowired
	private TeamFactory teamFactory;
	
	
	@Override
	public Results getResults() {
		
		Document document = parser.getHTMLDocument();
		Results results = new Results();
		
		List<Driver> drivers = driverFactory.createAndSortDriversByPoints(document, 10);
		List<Team> teams = teamFactory.createAndSortTeamsByPoints(document,5);
		results.setDrivers(drivers);
		results.setTeams(teams);
		
		return results;
	}

	
	public Formula1ResultsServiceImpl(HTMLParser parser,
			DriverFactory driverFactory, TeamFactory teamFactory) {
		super();
		this.parser = parser;
		this.driverFactory = driverFactory;
		this.teamFactory = teamFactory;
	}

	public Formula1ResultsServiceImpl(){
		super();
	}
	

	
	

}
