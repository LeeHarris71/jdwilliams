
## Building and running the application ##

Requires maven to build the application. From the command line, cd into the com.jdwilliams.formula1 directory and type the following:

	mvn package

Once built the application can be run by typing:

	java -jar target/com.jdwilliams.formula1-0.0.1-SNAPSHOT-jar-with-dependencies.jar

## Bugs ##

While testing there was an ad in the live page seemed to cause the parser to fail to read the HTML correctly. I tried a couple of parser and all had the same issue. The tests pass with clean HTML